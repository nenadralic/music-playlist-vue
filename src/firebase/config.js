import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const firebaseConfig = {
    apiKey: "AIzaSyB6lIVN6OUUSiq76tI3PVV7c5GlaEo7W2k",
    authDomain: "music-playlist-bce3a.firebaseapp.com",
    projectId: "music-playlist-bce3a",
    storageBucket: "music-playlist-bce3a.appspot.com",
    messagingSenderId: "98843771148",
    appId: "1:98843771148:web:9a33066a53425455ad0929"
  };


  //init firebase
  firebase.initializeApp(firebaseConfig)

  //init services
  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()
  const projectStorage = firebase.storage()


  //timestamp
  const timestamp = firebase.firestore.FieldValue.serverTimestamp

  export{
      projectFirestore,
      projectAuth,
      projectStorage,
      timestamp
  }